#[macro_use]
extern crate glium;
extern crate y4m;

use std::io;
use std::borrow::Cow;
use std::thread;
use std::time::Duration;
use glium::{DisplayBuild, Surface, VertexBuffer};
use glium::glutin::WindowBuilder;
use glium::texture::{RawImage2d, Texture2d, MipmapsOption, ClientFormat};
use glium::index::{NoIndices, PrimitiveType};
use glium::glutin::Event::KeyboardInput;
use glium::glutin::{VirtualKeyCode, ElementState};

fn main() {
    let mut inp = io::stdin();
    let dec = y4m::decode(&mut inp).unwrap();
    let width = dec.get_width() as u32;
    let height = dec.get_height() as u32;
    let framerate = dec.get_framerate();
    let framerate = framerate.num / framerate.den;
    println!("width: {:?}", width);
    println!("height: {:?}", height);
    println!("framerate: {:?} = {:?}", dec.get_framerate(), framerate);
    println!("colorspace: {:?}", dec.get_colorspace());

    let display = WindowBuilder::new()
                      .with_dimensions(width, height)
                      .build_glium()
                      .unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    // Flip the texture coordinates of the quad vertically, otherwise the image would be upside down:
    let quad = {
        let vertex1 = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 1.0],
        };
        let vertex2 = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 0.0],
        };
        let vertex3 = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 1.0],
        };
        let vertex4 = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 0.0],
        };

        vec![vertex1, vertex2, vertex3, vertex4]
    };
    let vertex_buffer = VertexBuffer::new(&display, &quad).unwrap();
    let indices = NoIndices(PrimitiveType::TriangleStrip);

    let program = program!(&display,
        // We don't need to convert the colours here, because we only upload grayscale
        // values into the RGB texture.
        // The Y luminance in YUV is exactly the same as the equation deriving the luminance from RGB.
        140 => {
            vertex: r#"
                #version 140

                in vec2 position;
                in vec2 tex_coords;
                out vec2 v_tex_coords;

                void main() {
                    v_tex_coords = tex_coords;
                    gl_Position = vec4(position, 0.0, 1.0);
                }
            "#,
            fragment: /*r#"
                #version 140

                in vec2 v_tex_coords;
                out vec3 color;

                uniform sampler2D y;
                uniform sampler2D u;
                uniform sampler2D v;

                void main() {
                    float y = texture(y, v_tex_coords).x;
                    float u = texture(u, v_tex_coords).x;
                    float v = texture(v, v_tex_coords).x;
                    // Fill the vec3 with (y,y,y): https://www.opengl.org/wiki/Data_Type_%28GLSL%29#Vector_constructors
                    vec3 c = vec3((y - 0.0625) * 1.164383);
                    // https://www.opengl.org/wiki/GLSL_Optimizations
                    color = c +                             // r, g, b coefficients for y component,
                        vec3(0.0, -0.39173, 2.017) * (u - 0.5);     // r, g, b coeffiecients for u component,
                        vec3(1.5958, -0.8129, 0.0) * (v - 0.5);     // for v component.
                }
            "#,*/
              /*r#"
                #version 140

                in vec2 v_tex_coords;
                out vec4 color;
              uniform sampler2D Ytex;
              uniform sampler2D Utex,Vtex;
              void main() {
                float r,g,b,y,u,v;
                y=texture(Ytex,v_tex_coords).r;
                u=texture(Utex,v_tex_coords).r;
                v=texture(Vtex,v_tex_coords).r;

                y=1.1643*(y-0.0625);
                u=u-0.5;
                v=v-0.5;

                r=y+1.5958*v;
                g=y-0.39173*u-0.81290*v;
                b=y+2.017*u;

                color=vec4(r,g,b,1.0);
              }"#,*/
             r#"
              #version 140
              uniform sampler2D y_tex;
              uniform sampler2D u_tex;
              uniform sampler2D v_tex;
              in vec2 v_tex_coords;
              out vec4 fragcolor;

              const vec3 R_cf = vec3(1.164383,  0.000000,  1.596027);
              const vec3 G_cf = vec3(1.164383, -0.391762, -0.812968);
              const vec3 B_cf = vec3(1.164383,  2.017232,  0.000000);
              const vec3 offset = vec3(-0.0625, -0.5, -0.5);

              float to_rgb(float c) {
                    float n;
                    if (c <= 0.04045) {
                        n = c / 12.92;
                    } else {
                        n = pow((c + 0.055) / 1.055, 2.4);
                    }
                    return n;
                }

              void main() {
                float y = texture(y_tex, v_tex_coords).r;
                float u = texture(u_tex, v_tex_coords).r;
                float v = texture(v_tex, v_tex_coords).r;
                vec3 yuv = vec3(y,u,v);
                yuv += offset;
                fragcolor = vec4(0.0, 0.0, 0.0, 1.0);
                fragcolor.r = to_rgb(dot(yuv, R_cf));
                fragcolor.g = to_rgb(dot(yuv, G_cf));
                fragcolor.b = to_rgb(dot(yuv, B_cf));
              }"#,
        },
    )
                      .unwrap();

    let mut playing = true;

    let mut dec = dec;
    'outer: loop {
        for ev in display.poll_events() {
            match ev {
                KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Escape)) |
                KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Return)) |
                KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::NumpadEnter)) |
                glium::glutin::Event::Closed => break 'outer,
                KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Space)) => {
                    playing = !playing
                }
                _ => (),
            }
        }

        if playing {
            match dec.read_frame() {
                Ok(frame) => {
                    let y = frame.get_y_plane();
                    // The sizes of the data and the texture must match.
                    let y = RawImage2d {
                        data: Cow::Borrowed(y),
                        width: width,
                        height: height,
                        format: ClientFormat::U8,
                    };
                    let y = Texture2d::with_mipmaps(&display, y, MipmapsOption::NoMipmap)
                                      .unwrap();

                    // The sizes of the other planes depends on the `Colorspace` used:
                    // http://wiki.multimedia.cx/index.php?title=YCbCr_4:2:0
                    // http://wiki.multimedia.cx/index.php?title=YUV4MPEG2
                    let u = frame.get_u_plane();
                    let u = RawImage2d {
                        data: Cow::Borrowed(u),
                        width: width / 2,
                        height: height / 2,
                        format: ClientFormat::U8,
                    };
                    let u = Texture2d::with_mipmaps(&display, u, MipmapsOption::NoMipmap)
                                      .unwrap();

                    let v = frame.get_v_plane();
                    let v = RawImage2d {
                        data: Cow::Borrowed(v),
                        width: width / 2,
                        height: height / 2,
                        format: ClientFormat::U8,
                    };
                    let v = Texture2d::with_mipmaps(&display, v, MipmapsOption::NoMipmap)
                                      .unwrap();

                    let mut target = display.draw();

                    target.draw(&vertex_buffer,
                                &indices,
                                &program,
                                &uniform! { y_tex: &y, u_tex: &u, v_tex: &v },
                                &Default::default())
                          .unwrap();
                    target.finish().unwrap();
                }
                _ => break,
            }
        }

        if !playing {
            // Sleep here to reduce CPU usage:
            thread::sleep(Duration::from_millis(100));
        }
    }
}
