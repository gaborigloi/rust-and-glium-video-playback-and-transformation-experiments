#[macro_use]
extern crate glium;
extern crate y4m;

use std::io;
use std::borrow::Cow;
use std::thread;
use std::time::Duration;
use glium::{DisplayBuild, Surface, VertexBuffer};
use glium::glutin::WindowBuilder;
use glium::texture::{RawImage2d, Texture2d, UncompressedFloatFormat, MipmapsOption, ClientFormat};
use glium::index::{NoIndices, PrimitiveType};
use glium::glutin::Event::KeyboardInput;
use glium::glutin::{VirtualKeyCode, ElementState};

fn main() {
    let mut inp = io::stdin();
    let dec = y4m::decode(&mut inp).unwrap();
    let width = dec.get_width() as u32;
    let height = dec.get_height() as u32;
    let framerate = dec.get_framerate();
    let framerate = framerate.num / framerate.den;
    println!("width: {:?}", width);
    println!("height: {:?}", height);
    println!("framerate: {:?} = {:?}", dec.get_framerate(), framerate);
    println!("colorspace: {:?}", dec.get_colorspace());

    let display = WindowBuilder::new()
                      .with_dimensions(width, height)
                      .build_glium()
                      .unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    // Flip the texture coordinates of the quad vertically, otherwise the image would be upside down:
    let quad = {
        let vertex1 = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 1.0],
        };
        let vertex2 = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 0.0],
        };
        let vertex3 = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 1.0],
        };
        let vertex4 = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 0.0],
        };

        vec![vertex1, vertex2, vertex3, vertex4]
    };
    let vertex_buffer = VertexBuffer::new(&display, &quad).unwrap();
    let indices = NoIndices(PrimitiveType::TriangleStrip);

    let program = {
        let vert = r#"
                #version 140

                in vec2 position;
                in vec2 tex_coords;
                out vec2 v_tex_coords;

                void main() {
                    v_tex_coords = tex_coords;
                    gl_Position = vec4(position, 0.0, 1.0);
                }
            "#;
        let frag = r#"
                #version 140

                in vec2 v_tex_coords;
                out float color;

                uniform sampler2D tex;

                void main() {
                    color = texture(tex, v_tex_coords).r;
                }
            "#;
        // The grayscale YUV4MPEG2 video is probably stored in sRGB already,
        // so we tell glium not to convert it int8o sRGB again:
        glium::Program::new(&display,
         glium::program::ProgramCreationInput::SourceCode {
             vertex_shader: vert,
             tessellation_control_shader: None,
             tessellation_evaluation_shader: None,
             geometry_shader: None,
             fragment_shader: frag,
             transform_feedback_varyings: None,
             outputs_srgb: true,
             uses_point_size: false
         }).unwrap()
         // But we will have to convert the output of the program into RGB
         // manually, if we need it for further processing, since it is in sRGB.
    };

    let mut playing = true;

    let mut dec = dec;
    'outer: loop {
        for ev in display.poll_events() {
            match ev {
                KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Escape))
                | KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Return))
                | KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::NumpadEnter))
                | glium::glutin::Event::Closed => break 'outer,
                KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::Space)) => { playing = !playing }
                _ => ()
            }
        }

        if playing {
            match dec.read_frame() {
                Ok(frame) => {
                    let y = frame.get_y_plane();
                    let y = RawImage2d {
                        data: Cow::Borrowed(y),
                        width: width,
                        height: height,
                        format: ClientFormat::U8,
                    };
                    let texture = Texture2d::with_format(&display,
                                                         y,
                                                         UncompressedFloatFormat::U8,
                                                         MipmapsOption::NoMipmap)
                                      .unwrap();

                    let mut target = display.draw();

                    target.draw(&vertex_buffer,
                                &indices,
                                &program,
                                &uniform! { tex: &texture },
                                &Default::default())
                          .unwrap();
                    target.finish().unwrap();
                },
                _ => break
            }
        }

        if !playing {
            // Sleep here to reduce CPU usage:
            thread::sleep(Duration::from_millis(100));
        }
    }
}
