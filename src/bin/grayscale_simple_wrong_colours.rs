// Apparently drawing with a quad is much faster than blitting the texture
// to the framebuffer without a quad:
// https://stackoverflow.com/questions/9209936/copy-texture-to-screen-buffer-without-drawing-quad-opengl

#[macro_use]
extern crate glium;
extern crate y4m;

use std::io;
use std::borrow::Cow;
use std::time::Duration;
use glium::{DisplayBuild, Surface, VertexBuffer};
use glium::glutin::WindowBuilder;
use glium::texture::{RawImage2d, Texture2d, UncompressedFloatFormat, MipmapsOption, ClientFormat};
use glium::index::{NoIndices, PrimitiveType};

// Maybe it is easier to understand when broken into well-named functions
// that do only one thing.
fn main() {
    let mut inp = io::stdin();
    let dec = y4m::decode(&mut inp).unwrap();
    let width = dec.get_width() as u32;
    let height = dec.get_height() as u32;
    let framerate = dec.get_framerate();
    let framerate = framerate.num / framerate.den;
    println!("width: {:?}", width);
    println!("height: {:?}", height);
    println!("framerate: {:?} = {:?}", dec.get_framerate(), framerate);
    println!("colorspace: {:?}", dec.get_colorspace());

    let display = WindowBuilder::new()
                      .with_dimensions(width, height)
                      .build_glium()
                      .unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    // Flip the texture coordinates of the quad vertically, otherwise the image would be upside down:
    let quad = {
        let vertex1 = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 1.0],
        };
        let vertex2 = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 0.0],
        };
        let vertex3 = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 1.0],
        };
        let vertex4 = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 0.0],
        };

        vec![vertex1, vertex2, vertex3, vertex4]
    };
    let vertex_buffer = VertexBuffer::new(&display, &quad).unwrap();
    let indices = NoIndices(PrimitiveType::TriangleStrip);

    let program = program!(&display,
        140 => {
            vertex: r#"
                #version 140

                in vec2 position;
                in vec2 tex_coords;
                out vec2 v_tex_coords;

                void main() {
                    v_tex_coords = tex_coords;
                    gl_Position = vec4(position, 0.0, 1.0);
                }
            "#,
            fragment: r#"
                #version 140

                in vec2 v_tex_coords;
                out float color;

                uniform sampler2D tex;

                void main() {
                    color = texture(tex, v_tex_coords).r;
                }
            "#,
        },
    ).unwrap();

    //let start = Instant::now();

    // Could add: separate threads, a buffer queue for decoded frames for the next 1s, uploading to
    // and swapping between two pixel buffers (upload to one in the current pass, draw from it next).
    // thread1 decoder --> queue --> thread2 display
    let mut dec = dec;
    'outer: while let Ok(frame) = dec.read_frame() {
        let y = frame.get_y_plane();
        let y = RawImage2d {
            data: Cow::Borrowed(y),
            width: width,
            height: height,
            format: ClientFormat::U8,
        };
        let texture = Texture2d::with_format(&display,
                                             y,
                                             UncompressedFloatFormat::U8,
                                             MipmapsOption::NoMipmap)
                          .unwrap();

        let mut target = display.draw();

        // The colours are wrong, they are too bright.
        target.draw(&vertex_buffer,
                    &indices,
                    &program,
                    &uniform! { tex: &texture },
                    &Default::default())
              .unwrap();
        target.finish().unwrap();
    }
}
